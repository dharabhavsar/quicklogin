package com.massmutual.poc.quicklogin.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.SocialProfile;
import com.massmutual.poc.quicklogin.R;
import com.massmutual.poc.quicklogin.service.FullContactService;
import com.massmutual.poc.quicklogin.service.ReversePhoneService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class RegForm4Activity extends AppCompatActivity {

    private static final String TAG = RegForm4Activity.class.getSimpleName();

    private ProgressDialog progressDialog;
    private TextInputEditText firstName;
    private TextInputEditText lastName;
    private TextInputEditText contactNumber;
    private TextInputEditText streetAddress;
    private TextInputEditText city;
    private TextInputEditText zipCode;
    private TextInputEditText state;
    private TextInputEditText country;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_form4);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventBus.getDefault().register(this);

        firstName = (TextInputEditText) findViewById(R.id.first_name_text);
        lastName = (TextInputEditText) findViewById(R.id.last_name_text);
        contactNumber = (TextInputEditText) findViewById(R.id.phone_number_text);
        streetAddress = (TextInputEditText) findViewById(R.id.street_address_text);
        city = (TextInputEditText) findViewById(R.id.city_text);
        zipCode = (TextInputEditText) findViewById(R.id.zip_code_text);
        state = (TextInputEditText) findViewById(R.id.state_text);
        country = (TextInputEditText) findViewById(R.id.country_text);

        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        if (intent != null) {
            String email = (String) intent.getExtras().get("EMAIL");
            FullContactService.fetchFullContactEmailInfo(email);
            progressDialog.setTitle("Fetching Data...");
            progressDialog.show();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.social_profiles);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResponseEvent(PersonResponse response) {
        EventBus.getDefault().removeStickyEvent(response);
        Log.e(TAG, "onResponseEvent: Success " + response.toString());
        Log.e(TAG, "onResponseEvent: Success " + response.getSocialProfiles().size());
        for(SocialProfile socialProfile : response.getSocialProfiles()) {
            Log.e(TAG, "onResponseEvent: " + socialProfile.toString());
        }

        // specify an adapter (see also next example)
        mAdapter = new SocialProfileAdapter(this, response.getSocialProfiles());
        mRecyclerView.setAdapter(mAdapter);

        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onErrorResponse(String error) {
        EventBus.getDefault().removeStickyEvent(error);
        Log.e(TAG, "onErrorResponse: " + error);
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Error Encountered... " + error);
        builder.show();
    }
}
