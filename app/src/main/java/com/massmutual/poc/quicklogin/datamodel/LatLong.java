package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class LatLong {

    public enum AccuracyType {
        RoofTop("RoofTop"),
        Street("Street"),
        PostalCode("PostalCode"),
        City("City"),
        State("State"),
        Country("Country");

        private String value;

        AccuracyType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @SerializedName("latitude")
    private float latitude;
    @SerializedName("longitude")
    private float longitude;
    @SerializedName("accuracy")
    private String accuracy;

    public LatLong(float latitude, float longitude, String accuracy) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getAccuracy() {
        for (AccuracyType type: AccuracyType.values()) {
            if (type.getValue().equals(accuracy))
                return accuracy;
        }
        return null;
    }
}
