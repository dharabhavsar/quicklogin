package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavsar, Dhara on 9/12/16.
 */
public class Name {

    @SerializedName("salutation")
    private String salutation;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("middle_name")
    private String middleName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("suffix")
    private String suffix;

    public Name(String salutation, String firstName, String middleName, String lastName, String suffix) {
        this.salutation = salutation;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.suffix = suffix;
    }

    public String getFullName() {
//         TODO : Add all the other fields
        return null;
    }

    public String getName() {
        return (firstName + " " + lastName);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
