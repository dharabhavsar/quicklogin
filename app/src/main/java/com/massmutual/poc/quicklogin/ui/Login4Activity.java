package com.massmutual.poc.quicklogin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.massmutual.poc.quicklogin.R;

/**
 * Created by Bhavsar, Dhara on 9/18/16.
 */
public class Login4Activity extends AppCompatActivity {

    private static final String TAG = Login4Activity.class.getSimpleName();

    private TextView email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login4);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email = (TextView) findViewById(R.id.email);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    * OnClickListeners
    */
    public void searchData(View view) {
        String email = this.email.getText().toString().trim();
        Log.e(TAG, "searchData: email ::: " + email);

//        Intent intent = new Intent(this, RegFormActivity.class);
        Intent intent = new Intent(this, RegForm4Activity.class);
        intent.putExtra("EMAIL", email);
        startActivity(intent);
    }
}
