package com.massmutual.poc.quicklogin.application;

import android.app.Application;

/**
 * Created by Bhavsar, Dhara on 9/1/16.
 */
public class QuickLogin extends Application {

    private AppController appController;

    @Override
    public void onCreate() {
        super.onCreate();
        appController = AppController.getInstance(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        appController.terminate();
    }
}
