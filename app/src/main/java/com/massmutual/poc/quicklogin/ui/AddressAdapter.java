package com.massmutual.poc.quicklogin.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.massmutual.poc.quicklogin.R;
import com.massmutual.poc.quicklogin.datamodel.Location;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Bhavsar, Dhara on 9/22/16.
 */
public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder>  {

    private static final String TAG = AddressAdapter.class.getSimpleName();
    private List<Location> locationList;
    private static Context context;

    public AddressAdapter(Context context, List<Location> locationList) {
        this.context = context;
        this.locationList = locationList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView streetAddressText;
        protected TextView cityText;
        protected TextView zipCodeText;
        protected TextView stateText;
        protected TextView countryText;

        public ViewHolder(View itemView) {
            super(itemView);
            streetAddressText = (TextView) itemView.findViewById(R.id.street_address_text);
            cityText = (TextView) itemView.findViewById(R.id.city_text);
            zipCodeText = (TextView) itemView.findViewById(R.id.zip_code_text);
            stateText = (TextView) itemView.findViewById(R.id.state_text);
            countryText = (TextView) itemView.findViewById(R.id.country_text);
        }

        public void onClickListener(final Location location) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick: item clicked: " + location.getStandardAddressLine1());
                    EventBus.getDefault().postSticky(location);
                }
            });
        }
    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_card, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(AddressAdapter.ViewHolder holder, int position) {
        Location location = locationList.get(position);
        holder.streetAddressText.setText(location.getStandardAddressLine1());
        holder.cityText.setText(location.getCity());
        holder.zipCodeText.setText(location.getPostalCode());
        holder.stateText.setText(location.getStateCode());
        holder.countryText.setText(location.getCountryCode());
        holder.onClickListener(location);
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }
}
