package com.massmutual.poc.quicklogin.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.massmutual.poc.quicklogin.BuildConfig;
import com.massmutual.poc.quicklogin.R;
import com.whitepages.proapi.api.client.Client;
import com.whitepages.proapi.api.client.FindException;
import com.whitepages.proapi.api.query.PhoneQuery;
import com.whitepages.proapi.api.response.Response;
import com.whitepages.proapi.api.response.ResponseMessages;
import com.whitepages.proapi.data.entity.Entity;
import com.whitepages.proapi.data.entity.Person;
import com.whitepages.proapi.data.entity.Phone;
import com.whitepages.proapi.data.message.Message;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RegFormActivity extends AppCompatActivity {

    private static final String TAG = RegFormActivity.class.getSimpleName();

    private TextInputEditText firstName;
    private TextInputEditText lastName;
    private TextInputLayout streetAddressLayout;
    private TextInputLayout cityLayout;
    private TextInputLayout zipCodeLayout;
    private TextInputLayout stateLayout;
    private TextInputLayout countryLayout;
    private TextInputEditText contactNumber;
    private TextInputEditText streetAddress;
    private TextInputEditText city;
    private TextInputEditText zipCode;
    private TextInputEditText state;
    private TextInputEditText country;
    private RecyclerView listOfAddresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_form);

        firstName = (TextInputEditText) findViewById(R.id.first_name_text);
        lastName = (TextInputEditText) findViewById(R.id.last_name_text);
        contactNumber = (TextInputEditText) findViewById(R.id.phone_number_text);
        streetAddressLayout = (TextInputLayout) findViewById(R.id.street_address);
        cityLayout = (TextInputLayout) findViewById(R.id.city);
        zipCodeLayout = (TextInputLayout) findViewById(R.id.zip_code);
        stateLayout = (TextInputLayout) findViewById(R.id.state);
        countryLayout = (TextInputLayout) findViewById(R.id.country);
        streetAddress = (TextInputEditText) findViewById(R.id.street_address_text);
        city = (TextInputEditText) findViewById(R.id.city_text);
        zipCode = (TextInputEditText) findViewById(R.id.zip_code_text);
        state = (TextInputEditText) findViewById(R.id.state_text);
        country = (TextInputEditText) findViewById(R.id.country_text);
//        listOfAddresses = (RecyclerView) findViewById(R.id.list_of_addresses);
//        listOfAddresses.setHasFixedSize(true);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        listOfAddresses.setLayoutManager(linearLayoutManager);

        String phoneNo;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                phoneNo = null;
            } else {
                phoneNo = extras.getString("PHONE_NUMBER");
            }
        } else {
            phoneNo = (String) savedInstanceState.getSerializable("PHONE_NUMBER");
        }

        if(phoneNo == null || phoneNo.equals("")) {
            Log.e(TAG, "onCreate: phoneNo obtained is null.");
            this.finish();
        }
        Log.e(TAG, "onCreate: phoneNo ::: " + phoneNo);

        new ReversePhoneTask().execute(phoneNo);

    }

    private class ReversePhoneTask extends AsyncTask<String, Void, Response<Phone>> {

        @Override
        protected Response<Phone> doInBackground(String... params) {
            String phoneNo = params[0];

            String apiKey = BuildConfig.WHITEPAGES_API_KEY;
            Log.e(TAG, "onCreate: apiKey ::: " + apiKey);
            Client client = new Client(apiKey);
            PhoneQuery phoneQuery = new PhoneQuery(phoneNo);
            Response<Phone> queryResponse = null;
            try {
                queryResponse = client.findPhones(phoneQuery);
            } catch (FindException e) {
                Log.e(TAG, "doInBackground: FindException: ", e);
                e.printStackTrace();
            }

            return queryResponse;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            Log.e(TAG, "onProgressUpdate: ");
            ProgressDialog progressDialog = new ProgressDialog(RegFormActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Response<Phone> queryResponse) {
            super.onPostExecute(queryResponse);

            List<Phone> phoneList = null;
            Phone phoneDetails = null;
            if(queryResponse == null) {
                Log.e(TAG, "onCreate: query failed to fetch a response.");
            } else {
                Log.e(TAG, "onCreate: query response is ::: " + queryResponse.toString());
                boolean success = queryResponse.isSuccess();
                Log.e(TAG, "onCreate: success = " + success);

                ResponseMessages messages = queryResponse.getResponseMessages();
                Iterator<Message> iter = messages.iterator();
                while (iter.hasNext()) {
                    Message m = iter.next();
                    Message.Code code = m.getCode();
                    String message = m.getMessage();
                    Message.Severity severity = m.getSeverity();

                    String mstr = String.format("#DBG: %s: %s (%s)", (code == null ? "null" : code.toString()), message, severity.toString());
                    Log.e(TAG, "onCreate: mstr = " + mstr);
                }
                phoneList = queryResponse.getResults();
                Log.e(TAG, "onCreate: phoneList size is ::: " + phoneList.size());
            }

            if(phoneList!= null && phoneList.size() == 1) {
                phoneDetails = phoneList.get(0);
                Log.e(TAG, "onCreate: phoneDetails is ::: " + phoneDetails.toString());
                Log.e(TAG, "onCreate: phoneDetails.Name is ::: " + phoneDetails.getName());
                Log.e(TAG, "onCreate: phoneDetails.PhoneNumber is ::: " + phoneDetails.getPhoneNumber());
                contactNumber.setText(phoneDetails.getPhoneNumber());
                Log.e(TAG, "onCreate: phoneDetails.Entities is ::: " + phoneDetails.getEntities().size());
                Log.e(TAG, "onPostExecute: associations " + phoneDetails.getAssociations().size());
                for(Entity e: phoneDetails.getEntities()) {
                    Log.e(TAG, "onCreate: e.Name is ::: " + e.getName());
                    Log.e(TAG, "onPostExecute: " + e.getAssociations().size());
//                    if(e.getName().equals(Person.Name.class.toString())) {
//                        Person.Name name = new
//                        firstName.setText();
//                    }
                }
//                Log.e(TAG, "onCreate: phoneDetails.Entities.0.Name is ::: " + phoneDetails.getEntities().get(0).getName());
//                Log.e(TAG, "onCreate: phoneDetails.Entities.0.People is ::: " + phoneDetails.getEntities().get(0).getPeople().size()); // NullPointerException
                Log.e(TAG, "onCreate: phoneDetails.Entities.0.Location is ::: " + phoneDetails.getEntities().get(0).getLocations().size());

                if(phoneDetails.getEntities().get(0).getLocations() != null &&
                        phoneDetails.getEntities().get(0).getLocations().size() == 1) {

                }

//                firstName = phoneDetails.getEntities().get(0).getB;
            }
        }
    }
}
