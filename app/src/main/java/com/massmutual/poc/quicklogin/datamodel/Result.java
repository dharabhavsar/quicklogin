package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class Result {

    @SerializedName("id")
    private Id id;
    @SerializedName("line_type")
    private String lineType;
    @SerializedName("belongs_to")
    private List<BelongsTo> belongsTo = new ArrayList<>();
    @SerializedName("associated_locations")
    private List<Location> associatedLocations = new ArrayList<>();
    @SerializedName("is_valid")
    private boolean isValid;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("country_calling_code")
    private String countryCallingCode;
    @SerializedName("carrier")
    private String carrier;
    @SerializedName("is_prepaid")
    private boolean isPrepaid;
    @SerializedName("is_connected")
    private boolean isConnected;

    public Result(Id id, String lineType, List<BelongsTo> belongsTo,
                  List<Location> associatedLocations, boolean isValid, String phoneNumber,
                  String countryCallingCode, String carrier, boolean isPrepaid,
                  boolean isConnected) {
        this.id = id;
        this.lineType = lineType;
        this.belongsTo = belongsTo;
        this.associatedLocations = associatedLocations;
        this.isValid = isValid;
        this.phoneNumber = phoneNumber;
        this.countryCallingCode = countryCallingCode;
        this.carrier = carrier;
        this.isPrepaid = isPrepaid;
        this.isConnected = isConnected;
    }

    public Id getId() {
        return id;
    }

    public String getLineType() {
        return lineType;
    }

    public List<BelongsTo> getBelongsTo() {
        return belongsTo;
    }

    public List<Location> getAssociatedLocations() {
        return associatedLocations;
    }

    public boolean isValid() {
        return isValid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCountryCallingCode() {
        return countryCallingCode;
    }

    public String getCarrier() {
        return carrier;
    }

    public boolean isPrepaid() {
        return isPrepaid;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
