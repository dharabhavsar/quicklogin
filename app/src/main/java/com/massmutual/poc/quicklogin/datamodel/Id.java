package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class Id {

    @SerializedName("key")
    private String key;
    @SerializedName("type")
    private String type;
    @SerializedName("durability")
    private String durability;

    public Id(String key, String type, String durability) {
        this.key = key;
        this.type = type;
        this.durability = durability;
    }

    public String getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public String getDurability() {
        return durability;
    }
}
