package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class BelongsTo {

    @SerializedName("id")
    private Id id;
    @SerializedName("name")
    private String name;
    @SerializedName("names")
    private List<Name> names;
    @SerializedName("locations")
    private List<Location> locations = new ArrayList<>();
    @SerializedName("phones")
    private List<Phone> phones = new ArrayList<>();
    @SerializedName("urls")
    private List<Url> urls = new ArrayList<>();

    public BelongsTo(Id id, List<Name> names, List<Location> locations, List<Phone> phones, List<Url> urls) {
        this.id = id;
        this.names = names;
        this.locations = locations;
        this.phones = phones;
        this.urls = urls;
    }

    public Id getId() {
        return id;
    }

    public List<Name> getNames() {
        return names;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public List<Url> getUrls() {
        return urls;
    }

    public String getName() {
        return name;
    }
}
