package com.massmutual.poc.quicklogin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.massmutual.poc.quicklogin.R;

/**
 * Created by Bhavsar, Dhara on 9/18/16.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private TextView phoneDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        phoneDetail = (TextView) findViewById(R.id.phone_number);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    * OnClickListeners
    */
    public void searchData(View view) {
        String phoneNo = phoneDetail.getText().toString().trim();
        Log.e(TAG, "searchData: phoneNo ::: " + phoneNo);

//        Intent intent = new Intent(this, RegFormActivity.class);
        Intent intent = new Intent(this, RegFormActivity.class);
        intent.putExtra("PHONE_NUMBER", phoneNo);
        startActivity(intent);
    }
}
