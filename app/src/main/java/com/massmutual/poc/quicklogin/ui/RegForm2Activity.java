package com.massmutual.poc.quicklogin.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.massmutual.poc.quicklogin.BuildConfig;
import com.massmutual.poc.quicklogin.R;
import com.massmutual.poc.quicklogin.datamodel.BelongsTo;
import com.massmutual.poc.quicklogin.datamodel.Location;
import com.massmutual.poc.quicklogin.datamodel.Name;
import com.massmutual.poc.quicklogin.datamodel.ProfileData;
import com.massmutual.poc.quicklogin.datamodel.Result;
import com.massmutual.poc.quicklogin.datamodel.ReversePhoneResponse;
import com.massmutual.poc.quicklogin.service.ReversePhoneService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class RegForm2Activity extends AppCompatActivity {

    private static final String TAG = RegForm2Activity.class.getSimpleName();

    private ProgressDialog progressDialog;
    private TextInputEditText firstName;
    private TextInputEditText lastName;
    private TextInputLayout streetAddressLayout;
    private TextInputLayout cityLayout;
    private TextInputLayout zipCodeLayout;
    private TextInputLayout stateLayout;
    private TextInputLayout countryLayout;
    private TextInputEditText contactNumber;
    private TextInputEditText streetAddress;
    private TextInputEditText city;
    private TextInputEditText zipCode;
    private TextInputEditText state;
    private TextInputEditText country;
    private RecyclerView listOfAddresses;
    private RecyclerView listOfImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_form2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventBus.getDefault().register(this);

        firstName = (TextInputEditText) findViewById(R.id.first_name_text);
        lastName = (TextInputEditText) findViewById(R.id.last_name_text);
        contactNumber = (TextInputEditText) findViewById(R.id.phone_number_text);
        streetAddressLayout = (TextInputLayout) findViewById(R.id.street_address);
        cityLayout = (TextInputLayout) findViewById(R.id.city);
        zipCodeLayout = (TextInputLayout) findViewById(R.id.zip_code);
        stateLayout = (TextInputLayout) findViewById(R.id.state);
        countryLayout = (TextInputLayout) findViewById(R.id.country);
        streetAddress = (TextInputEditText) findViewById(R.id.street_address_text);
        city = (TextInputEditText) findViewById(R.id.city_text);
        zipCode = (TextInputEditText) findViewById(R.id.zip_code_text);
        state = (TextInputEditText) findViewById(R.id.state_text);
        country = (TextInputEditText) findViewById(R.id.country_text);
        listOfAddresses = (RecyclerView) findViewById(R.id.list_of_addresses);
        listOfAddresses.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listOfAddresses.setLayoutManager(linearLayoutManager);
        listOfImages = (RecyclerView) findViewById(R.id.list_of_fbProfiles);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        listOfImages.setLayoutManager(linearLayoutManager2);

        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        if (intent != null) {
            String phoneNo = (String) intent.getExtras().get(getString(R.string.phone_number));
            ReversePhoneService.fetchPhoneData("https://proapi.whitepages.com/2.2/phone.json?api_key="
                    + BuildConfig.WHITEPAGES_API_KEY + "&phone_number=" + phoneNo);
            progressDialog.setTitle("Fetching Data...");
            progressDialog.show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done_menu, menu);

        MenuItem actionDone = menu.findItem(R.id.action_done);

        actionDone.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                ProfileData profileData = new ProfileData(firstName.getText().toString(), lastName.getText().toString(),
                        contactNumber.getText().toString(), streetAddress.getText().toString(),
                        city.getText().toString(), zipCode.getText().toString(),
                        state.getText().toString(), country.getText().toString());
                String profileDataStr = profileData.toJson();
                Log.e(TAG, "onMenuItemClick: ..................... " + profileDataStr);
                Intent intent = new Intent(getApplicationContext(), ProfileDisplayActivity.class);
                intent.putExtra(getString(R.string.data), profileDataStr);
                startActivity(intent);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResponseEvent(ReversePhoneResponse response) {
        EventBus.getDefault().removeStickyEvent(response);
        Log.e(TAG, "onResponseEvent: Success" + response.toString());

        List<Result> resultList = response.getResults();

        if(resultList.size() == 1) {
            Result result = resultList.get(0);
            List<BelongsTo> belongsToList = result.getBelongsTo();

            if(belongsToList.size() == 1) {
                BelongsTo belongsTo = belongsToList.get(0);

//                TODO : can check on line_type too

                List<Name> nameList = belongsTo.getNames();
                if(nameList != null) {
                    if (nameList.size() == 1) {
                        Name name = nameList.get(0);
                        firstName.setText(name.getFirstName());
                        lastName.setText(name.getLastName());
                    } else {
                        Log.e(TAG, "onResponseEvent: NameList more than one..." + nameList.size());
                    }
                } else {
                    Log.e(TAG, "onResponseEvent: NameList is null");
                    firstName.setText(belongsTo.getName());
                }

                List<Location> locationList = belongsTo.getLocations();
                if(locationList != null) {
                    Log.e(TAG, "onResponseEvent: " + locationList.size());
                    if (locationList.size() == 1) {
                        streetAddressLayout.setVisibility(View.VISIBLE);
                        cityLayout.setVisibility(View.VISIBLE);
                        zipCodeLayout.setVisibility(View.VISIBLE);
                        stateLayout.setVisibility(View.VISIBLE);
                        countryLayout.setVisibility(View.VISIBLE);
                        listOfAddresses.setVisibility(View.GONE);
                        Location location = locationList.get(0);
                        streetAddress.setText(location.getStandardAddressLine1());
                        city.setText(location.getCity());
                        zipCode.setText(location.getPostalCode());
                        state.setText(location.getStateCode());
                        country.setText(location.getCountryCode());
                    } else {
                        Log.e(TAG, "onResponseEvent: LocationList in belongsTo more than one..." + locationList.size());
                        streetAddressLayout.setVisibility(View.GONE);
                        cityLayout.setVisibility(View.GONE);
                        zipCodeLayout.setVisibility(View.GONE);
                        stateLayout.setVisibility(View.GONE);
                        countryLayout.setVisibility(View.GONE);
                        listOfAddresses.setVisibility(View.VISIBLE);
                        AddressAdapter addressAdapter = new AddressAdapter(this, locationList);
                        listOfAddresses.setAdapter(addressAdapter);
                    }
                } else {
                    Log.e(TAG, "onResponseEvent: LocationList in belongsTo is null");
                    locationList = result.getAssociatedLocations();
                    getLocation(locationList);
                }

                contactNumber.setText(result.getPhoneNumber());
            } else {
                Log.e(TAG, "onResponseEvent: BelongsToList more than one person..." + belongsToList.size());
                if(belongsToList.size() == 0) {
                    Log.e(TAG, "onResponseEvent: belongsTo is null or empty");
                    List<Location> locationList = result.getAssociatedLocations();
                    getLocation(locationList);
                }
            }
        } else {
            Log.e(TAG, "onResponseEvent: ResultList more than one..." + resultList.size());
        }

        progressDialog.dismiss();
    }

    private void getLocation(List<Location> locationList) {
        if(locationList != null) {
            if (locationList.size() == 1) {
                Location location = locationList.get(0);
                streetAddress.setText(location.getStandardAddressLine1());
                city.setText(location.getCity());
                zipCode.setText(location.getPostalCode());
                state.setText(location.getStateCode());
                country.setText(location.getCountryCode());
            } else {
                Log.e(TAG, "onResponseEvent: LocationList in result more than one..." + locationList.size());
            }
        } else {
            Log.e(TAG, "onResponseEvent: LocationList in result is null");
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onErrorResponse(String error) {
        Log.e(TAG, "onErrorResponse: " + error);
        EventBus.getDefault().removeStickyEvent(error);
        progressDialog.dismiss();
//        AlertDialog alertDialog = new AlertDialog(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onAddressEvent(Location location) {
        Log.e(TAG, "onAddressEvent: " + location.getStandardAddressLine1());
        EventBus.getDefault().removeStickyEvent(location);
        streetAddressLayout.setVisibility(View.VISIBLE);
        cityLayout.setVisibility(View.VISIBLE);
        zipCodeLayout.setVisibility(View.VISIBLE);
        stateLayout.setVisibility(View.VISIBLE);
        countryLayout.setVisibility(View.VISIBLE);
        listOfAddresses.setVisibility(View.GONE);
        streetAddress.setText(location.getStandardAddressLine1());
        city.setText(location.getCity());
        zipCode.setText(location.getPostalCode());
        state.setText(location.getStateCode());
        country.setText(location.getCountryCode());
    }

}
