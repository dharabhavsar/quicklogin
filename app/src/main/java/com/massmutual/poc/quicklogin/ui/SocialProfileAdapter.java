package com.massmutual.poc.quicklogin.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fullcontact.api.libs.fullcontact4j.http.person.model.SocialProfile;
import com.massmutual.poc.quicklogin.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhavsar, Dhara on 10/3/16.
 */

public class SocialProfileAdapter extends RecyclerView.Adapter<SocialProfileAdapter.ViewHolder> {

    private List<SocialProfile> socialProfileList;
    private static Context context;

    public SocialProfileAdapter(Context context, List<SocialProfile> socialProfiles) {
        this.context = context;
        socialProfileList = new ArrayList<>();
        for(SocialProfile socialProfile: socialProfiles) {
            if(socialProfile.getTypeName().equalsIgnoreCase("Facebook") ||
                    socialProfile.getTypeName().equalsIgnoreCase("LinkedIn"))
            socialProfileList.add(socialProfile);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView mTextView;
        protected TextView mFbImg;
        protected TextView mLinkedInImg;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.social_profile_url);
            mFbImg = (TextView) itemView.findViewById(R.id.profile_fb_img);
            mLinkedInImg = (TextView) itemView.findViewById(R.id.profile_linkedIn_img);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.social_profiles_card, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SocialProfile socialProfile = socialProfileList.get(position);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf" );
//        holder.mTextView.setText(socialProfile.getUrl());
        if(socialProfile.getTypeName().equalsIgnoreCase("Facebook")) {
            holder.mFbImg.setTypeface(font);
            if(Build.VERSION.SDK_INT <= 23) {
                holder.mTextView.setText(Html.fromHtml(context.getString(R.string.fb_url, socialProfile.getUrl())));
            } else {
                holder.mTextView.setText(Html.fromHtml(context.getString(R.string.fb_url, socialProfile.getUrl()), 0));
            }
            holder.mLinkedInImg.setVisibility(View.GONE);
        } else if(socialProfile.getTypeName().equalsIgnoreCase("LinkedIn")) {
            holder.mFbImg.setVisibility(View.GONE);
            if(Build.VERSION.SDK_INT <= 23) {
                holder.mTextView.setText(Html.fromHtml(context.getString(R.string.linkedIn_url, socialProfile.getUrl())));
            } else {
                holder.mTextView.setText(Html.fromHtml(context.getString(R.string.linkedIn_url, socialProfile.getUrl()), 0));
            }
            holder.mLinkedInImg.setTypeface(font);
        }
        holder.mTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public int getItemCount() {
        return socialProfileList.size();
    }
}
