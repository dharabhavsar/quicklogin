package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class Url {

    @SerializedName("url")
    private String url;
    @SerializedName("display_name")
    private Object displayName;
    @SerializedName("type")
    private String type;
    @SerializedName("extended_type")
    private Object extendedType;
    @SerializedName("sharing_level")
    private Object sharingLevel;

    public Url(String url, Object displayName, String type, Object extendedType, Object sharingLevel) {
        this.url = url;
        this.displayName = displayName;
        this.type = type;
        this.extendedType = extendedType;
        this.sharingLevel = sharingLevel;
    }

    public String getUrl() {
        return url;
    }

    public Object getDisplayName() {
        return displayName;
    }

    public String getType() {
        return type;
    }

    public Object getExtendedType() {
        return extendedType;
    }

    public Object getSharingLevel() {
        return sharingLevel;
    }
}
