package com.massmutual.poc.quicklogin.service;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by Bhavsar, Dhara on 9/22/16.
 */

public class CallBackInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    CallBackInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
}
