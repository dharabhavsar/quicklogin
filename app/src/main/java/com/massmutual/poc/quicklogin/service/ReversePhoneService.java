package com.massmutual.poc.quicklogin.service;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fullcontact.api.libs.fullcontact4j.FullContact;
import com.fullcontact.api.libs.fullcontact4j.FullContactException;
import com.fullcontact.api.libs.fullcontact4j.http.FCCallback;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonRequest;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.google.gson.Gson;
import com.massmutual.poc.quicklogin.application.AppController;
import com.massmutual.poc.quicklogin.datamodel.ReversePhoneResponse;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class ReversePhoneService {

    private static final String TAG = ReversePhoneService.class.getSimpleName();

    public static void fetchPhoneData(String url) {

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "fetchPhoneData: Reverse Phone Info fetched from Server");
                        Log.d(TAG, response);

                        ReversePhoneResponse reversePhoneResponse = new Gson().fromJson(response, ReversePhoneResponse.class);

//                        TODO : add event class or a constant
                        EventBus.getDefault().postSticky(reversePhoneResponse);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "fetchPhoneData: Failed to fetch info from Server");
                        Log.e(TAG, error.toString());
                        EventBus.getDefault().postSticky("ErrorResponse");
                    }
                });

        AppController.getInstance().addToRequestQueue(request);
    }
}
