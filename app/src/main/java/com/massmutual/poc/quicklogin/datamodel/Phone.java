package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class Phone {

    public enum LineTypes {
        Mobile("Mobile"),
        Landline("Landline"),
        FixedVOIP("FixedVOIP"),
        NonFixedVOIP("NonFixedVOIP"),
        Voicemail("Voicemail"),
        TollFree("TollFree"),
        Premium("Premium"),
        Other("Other"),
        Unknown("Unknown");

        private String value;

        LineTypes(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @SerializedName("id")
    private Id id;
    @SerializedName("line_type")
    private String lineType;
    @SerializedName("belongs_to")
    private Object belongsTo;
    @SerializedName("associated_locations")
    private Object associatedLocations;
    @SerializedName("is_valid")
    private boolean isValid;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("country_calling_code")
    private String countryCallingCode;
    @SerializedName("carrier")
    private Object carrier;
    @SerializedName("is_prepaid")
    private Object isPrepaid;
    @SerializedName("is_connected")
    private Object isConnected;
    @SerializedName("valid_for")
    private Object validFor;
    @SerializedName("contact_type")
    private String contactType;
    @SerializedName("contact_creation_date")
    private Object contactCreationDate;

    public Phone(Id id, String lineType, Object belongsTo, Object associatedLocations,
                 boolean isValid, String phoneNumber, String countryCallingCode, Object carrier,
                 Object isPrepaid, Object isConnected, Object validFor, String contactType,
                 Object contactCreationDate) {
        this.id = id;
        this.lineType = lineType;
        this.belongsTo = belongsTo;
        this.associatedLocations = associatedLocations;
        this.isValid = isValid;
        this.phoneNumber = phoneNumber;
        this.countryCallingCode = countryCallingCode;
        this.carrier = carrier;
        this.isPrepaid = isPrepaid;
        this.isConnected = isConnected;
        this.validFor = validFor;
        this.contactType = contactType;
        this.contactCreationDate = contactCreationDate;
    }

    public Id getId() {
        return id;
    }

    public String getLineType() {
        for (LineTypes type: LineTypes.values()) {
            if (type.getValue().equals(lineType))
                return lineType;
        }
        return null;
    }

    public Object getBelongsTo() {
        return belongsTo;
    }

    public Object getAssociatedLocations() {
        return associatedLocations;
    }

    public boolean isValid() {
        return isValid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCountryCallingCode() {
        return countryCallingCode;
    }

    public Object getCarrier() {
        return carrier;
    }

    public Object getIsPrepaid() {
        return isPrepaid;
    }

    public Object getIsConnected() {
        return isConnected;
    }

    public Object getValidFor() {
        return validFor;
    }

    public String getContactType() {
        return contactType;
    }

    public Object getContactCreationDate() {
        return contactCreationDate;
    }
}
