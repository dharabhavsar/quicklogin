package com.massmutual.poc.quicklogin.application;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.massmutual.poc.quicklogin.BuildConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Bhavsar, Dhara on 9/1/16.
 */
public class AppController {

    private static final String TAG = AppController.class.getSimpleName();

    private static AppController mSingleInstance;
    private static Context mContext;
    private static RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public AppController(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
        mImageLoader = getImageLoader();
        EventBus.getDefault().register(this);

        FacebookSdk.sdkInitialize(mContext);
        AppEventsLogger.activateApp((Application)mContext, BuildConfig.FACEBOOK_API_KEY);
    }

    public static synchronized AppController getInstance(Context context) {
        if (mSingleInstance == null) {
            mSingleInstance = new AppController(context);
        }
        return mSingleInstance;
    }

    public static AppController getInstance() {
        return mSingleInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        request.setRetryPolicy(new DefaultRetryPolicy(8000,
                2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(TAG);
        getRequestQueue().add(request);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(getRequestQueue(),
                    new BitmapLruCache());
        }
        return this.mImageLoader;
    }

    public ImageLoader getmImageLoader() {
        return mImageLoader;
    }

    public void terminate() {
        mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
        mRequestQueue.stop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onTestEvent(String event) {
        Log.d(TAG, "onTestEvent: EVENTBUS Test... " + event);
        Toast.makeText(mContext, "From Loading "+event, Toast.LENGTH_SHORT).show();
    }
}
