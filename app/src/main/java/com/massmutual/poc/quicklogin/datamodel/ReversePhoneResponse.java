package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class ReversePhoneResponse {
    @SerializedName("results")
    private List<Result> results = new ArrayList<>();
    @SerializedName("messages")
    private List<Object> messages = new ArrayList<>();

    public ReversePhoneResponse(List<Result> results, List<Object> messages) {
        this.results = results;
        this.messages = messages;
    }

    public List<Result> getResults() {
        return results;
    }

    public List<Object> getMessages() {
        return messages;
    }
}
