package com.massmutual.poc.quicklogin.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.massmutual.poc.quicklogin.BuildConfig;
import com.massmutual.poc.quicklogin.R;
import com.massmutual.poc.quicklogin.service.ReversePhoneService;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Bhavsar, Dhara on 10/17/16.
 */

public class SelectFbPicActivity extends AppCompatActivity {

    private static final String TAG = SelectFbPicActivity.class.getSimpleName();

    private ProgressDialog progressDialog;
    private RecyclerView listOfImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_form2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventBus.getDefault().register(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listOfImages = (RecyclerView) findViewById(R.id.list_of_fbProfiles);
        listOfImages.setLayoutManager(linearLayoutManager);

        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        if (intent != null) {
            String phoneNo = (String) intent.getExtras().get("PHONE_NUMBER");

            progressDialog.setTitle("Fetching Data...");
            progressDialog.show();
        }
    }
}
