package com.massmutual.poc.quicklogin.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.massmutual.poc.quicklogin.R;
import com.massmutual.poc.quicklogin.datamodel.ProfileData;

/**
 * Created by Bhavsar, Dhara on 10/17/16.
 */

public class ProfileDisplayActivity extends AppCompatActivity {

    private static final String TAG = ProfileDisplayActivity.class.getSimpleName();

    private ProgressDialog progressDialog;
    private TextView firstName;
    private TextView lastName;
    private TextView contactNumber;
    private TextView streetAddress;
    private TextView city;
    private TextView zipCode;
    private TextView state;
    private TextView country;
    private LinearLayout layout1;
    private LinearLayout layout2;
    private LinearLayout layout3;
    private LinearLayout layout4;
    private LinearLayout layout5;
    private LinearLayout layout6;
    private LinearLayout layout7;
    private LinearLayout layout8;
    private RecyclerView listOfImages;
    ProfileData profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_display);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.close_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firstName = (TextView) findViewById(R.id.textView2);
        lastName = (TextView) findViewById(R.id.textView4);
        contactNumber = (TextView) findViewById(R.id.textView6);
        streetAddress = (TextView) findViewById(R.id.textView8);
        city = (TextView) findViewById(R.id.textView10);
        zipCode = (TextView) findViewById(R.id.textView12);
        state = (TextView) findViewById(R.id.textView14);
        country = (TextView) findViewById(R.id.textView16);
        layout1 = (LinearLayout) findViewById(R.id.rel_layout_1);
        layout2 = (LinearLayout) findViewById(R.id.rel_layout_2);
        layout3 = (LinearLayout) findViewById(R.id.rel_layout_3);
        layout4 = (LinearLayout) findViewById(R.id.rel_layout_4);
        layout5 = (LinearLayout) findViewById(R.id.rel_layout_5);
        layout6 = (LinearLayout) findViewById(R.id.rel_layout_6);
        layout7 = (LinearLayout) findViewById(R.id.rel_layout_7);
        layout8 = (LinearLayout) findViewById(R.id.rel_layout_8);

        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        if (intent != null) {
            String profileDataStr = intent.getExtras().getString(getString(R.string.data));
            profileData = ProfileData.fromJson(profileDataStr);
        }

        if (profileData != null) {
            firstName.setText(profileData.getFirstName());
            lastName.setText(profileData.getLastName());
            contactNumber.setText(profileData.getContactNumber());
            streetAddress.setText(profileData.getStreetAddress());
            city.setText(profileData.getCity());
            zipCode.setText(profileData.getZipCode());
            state.setText(profileData.getState());
            country.setText(profileData.getCountry());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavUtils.navigateUpFromSameTask(this);
        return true;
//        return super.onSupportNavigateUp();
    }
}
