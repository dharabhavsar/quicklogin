package com.massmutual.poc.quicklogin.service;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fullcontact.api.libs.fullcontact4j.FullContact;
import com.fullcontact.api.libs.fullcontact4j.FullContactException;
import com.fullcontact.api.libs.fullcontact4j.http.FCCallback;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonRequest;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.massmutual.poc.quicklogin.BuildConfig;
import com.massmutual.poc.quicklogin.application.AppController;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Bhavsar, Dhara on 9/30/16.
 */

public class FullContactService {

    private static final String TAG = FullContactService.class.getSimpleName();
    private static PersonResponse personResponse;

    public static void fetchFullContactData(String url) {

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "fetchFullContactData: Reverse Phone Info fetched from Server");
                        Log.d(TAG, response);

//                        ReversePhoneResponse reversePhoneResponse = new Gson().fromJson(response, ReversePhoneResponse.class);

//                        TODO : add event class or a constant
//                        EventBus.getDefault().postSticky(reversePhoneResponse);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "fetchFullContactData: Failed to fetch info from Server");
                        Log.e(TAG, error.toString());
                        EventBus.getDefault().postSticky("ErrorResponse");
                    }
                });

        AppController.getInstance().addToRequestQueue(request);
    }

    public static void fetchFullContactInfo(String phoneNo) {
        FullContact fullContact = FullContact.withApiKey(BuildConfig.FULLCONTACT_API_KEY)
                .setUserAgent("testPhone")
                .build();
        final PersonRequest personRequest = fullContact.buildPersonRequest()
//                .webhookUrl("http://requestb.in/1lvddm71")
//                .webhookBody(true)
//                .phone("+13037170414")
                .phone(phoneNo)
                .build();
        try {
            fullContact.sendRequest(personRequest, new FCCallback<PersonResponse>() {
                @Override
                public void success(PersonResponse res) {
                    Log.e(TAG, "success: " + res.toString());
                    personResponse = res;
                    Log.e(TAG, "fetchFullContactInfo: " + personResponse.toString());
                    EventBus.getDefault().postSticky(personResponse);
                }

                @Override
                public void failure(FullContactException e) {
                    Log.e(TAG, "failure: ", e);
                    EventBus.getDefault().postSticky("ErrorResponse");
                }
            });
//            PersonResponse personResponse = fullContact.sendRequest(personRequest);
        } catch (Exception e) {
            Log.e(TAG, "fetchFullContactInfo: ", e);
            EventBus.getDefault().postSticky("ErrorResponse");
        }
        fullContact.shutdown();
    }

    public static void fetchFullContactEmailInfo(String email) {
        FullContact fullContact = FullContact.withApiKey(BuildConfig.FULLCONTACT_API_KEY)
                .setUserAgent("testEmail")
                .build();
        final PersonRequest personRequest = fullContact.buildPersonRequest()
//                .email("bart@fullcontact.com")
                .email(email)
                .build();
        try {
            fullContact.sendRequest(personRequest, new FCCallback<PersonResponse>() {
                @Override
                public void success(PersonResponse res) {
                    Log.e(TAG, "success: " + res.toString());
                    personResponse = res;
                    Log.e(TAG, "fetchFullContactInfo: " + personResponse.toString());
                    EventBus.getDefault().postSticky(personResponse);
                }

                @Override
                public void failure(FullContactException e) {
                    Log.e(TAG, "failure: ", e);
                    EventBus.getDefault().postSticky("ErrorResponse");
                }
            });
//            PersonResponse personResponse = fullContact.sendRequest(personRequest);
        } catch (Exception e) {
            Log.e(TAG, "fetchFullContactInfo: ", e);
            EventBus.getDefault().postSticky("ErrorResponse");
        }
        fullContact.shutdown();
    }
}
