package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavsar, Dhara on 9/2/16.
 */
public class Location {

    public enum LocationType {
        LatLong("LatLong"),
        Address("Address"),
        State("State"),
        City("City"),
        County("County"),
        Neighborhood("Neighborhood"),
        PostalCode("PostalCode"),
        Country("Country"),
        ZipPlus4("ZipPlus4"),
        CityPostalCode("CityPostalCode");

        private String value;

        LocationType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum NotReceivingMailReason {
        Vacant("Vacant"),
        NewConstruction("New Construction"),
        Rural("Rural");

        private String value;

        NotReceivingMailReason(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum DeliveryPoint {
        CommercialMailDrop("CommercialMailDrop"),
        POBoxThrowback("POBoxThrowback"),
        POBox("POBox"),
        MultiUnit("MultiUnit"),
        SingleUnit("SingleUnit");

        private String value;

        DeliveryPoint(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum AddressType {
        Firm("Firm"),
        GeneralDelivery("GeneralDelivery"),
        Highrise("Highrise"),
        POBox("POBox"),
        RuralRoute("RuralRoute"),
        Street("Street");

        private String value;

        AddressType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @SerializedName("id")
    private Id id;
    @SerializedName("type")
    private String type;
    @SerializedName("valid_for")
    private Object validFor;
    @SerializedName("legal_entities_at")
    private Object legalEntitiesAt;
    @SerializedName("city")
    private String city;
    @SerializedName("postal_code")
    private String postalCode;
    @SerializedName("zip4")
    private String zip4;
    @SerializedName("state_code")
    private String stateCode;
    @SerializedName("country_code")
    private String countryCode;
    @SerializedName("is_receiving_mail")
    private boolean isReceivingMail;
    @SerializedName("not_receiving_mail_reason")
    private Object notReceivingMailReason;
    @SerializedName("usage")
    private Object usage;
    @SerializedName("delivery_point")
    private Object deliveryPoint;
    @SerializedName("address_type")
    private Object addressType;
    @SerializedName("lat_long")
    private LatLong latLong;
    @SerializedName("is_deliverable")
    private Object isDeliverable;
    @SerializedName("standard_address_line1")
    private String standardAddressLine1;
    @SerializedName("standard_address_line2")
    private String standardAddressLine2;
    @SerializedName("is_historical")
    private boolean isHistorical;
    @SerializedName("contact_creation_date")
    private Object contactCreationDate;

    public Location(Id id, String type, Object validFor, Object legalEntitiesAt, String city,
                    String postalCode, String zip4, String stateCode, String countryCode,
                    boolean isReceivingMail, Object notReceivingMailReason, Object usage,
                    Object deliveryPoint, Object addressType, LatLong latLong, Object isDeliverable,
                    String standardAddressLine1, String standardAddressLine2, boolean isHistorical,
                    Object contactCreationDate) {
        this.id = id;
        this.type = type;
        this.validFor = validFor;
        this.legalEntitiesAt = legalEntitiesAt;
        this.city = city;
        this.postalCode = postalCode;
        this.zip4 = zip4;
        this.stateCode = stateCode;
        this.countryCode = countryCode;
        this.isReceivingMail = isReceivingMail;
        this.notReceivingMailReason = notReceivingMailReason;
        this.usage = usage;
        this.deliveryPoint = deliveryPoint;
        this.addressType = addressType;
        this.latLong = latLong;
        this.isDeliverable = isDeliverable;
        this.standardAddressLine1 = standardAddressLine1;
        this.standardAddressLine2 = standardAddressLine2;
        this.isHistorical = isHistorical;
        this.contactCreationDate = contactCreationDate;
    }

    public Id getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Object getValidFor() {
        return validFor;
    }

    public Object getLegalEntitiesAt() {
        return legalEntitiesAt;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getZip4() {
        return zip4;
    }

    public String getStateCode() {
        return stateCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public boolean isReceivingMail() {
        return isReceivingMail;
    }

    public Object getNotReceivingMailReason() {
        return notReceivingMailReason;
    }

    public Object getUsage() {
        return usage;
    }

    public Object getDeliveryPoint() {
        return deliveryPoint;
    }

    public Object getAddressType() {
        return addressType;
    }

    public LatLong getLatLong() {
        return latLong;
    }

    public Object getIsDeliverable() {
        return isDeliverable;
    }

    public String getStandardAddressLine1() {
        return standardAddressLine1;
    }

    public String getStandardAddressLine2() {
        return standardAddressLine2;
    }

    public boolean isHistorical() {
        return isHistorical;
    }

    public Object getContactCreationDate() {
        return contactCreationDate;
    }
}
