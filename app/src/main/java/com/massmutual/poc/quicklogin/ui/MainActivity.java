package com.massmutual.poc.quicklogin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.massmutual.poc.quicklogin.BuildConfig;
import com.massmutual.poc.quicklogin.R;

import net.hockeyapp.android.CrashManager;

/**
 * Created by Bhavsar, Dhara on 8/29/16.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {

        CrashManager.register(this, BuildConfig.HOCKEYAPP_APP_ID);

        super.onResume();
    }

    /*
        * On click listeners
        * */
    public void start_demo_1(View view) {
        startActivity(new Intent(this, Login2Activity.class));
    }

    public void start_demo_2(View view) {
        startActivity(new Intent(this, Login3Activity.class));
    }

    public void start_demo_0(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void start_demo_3(View view) {
        startActivity(new Intent(this, Login4Activity.class));
    }
}
