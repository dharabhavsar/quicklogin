package com.massmutual.poc.quicklogin.datamodel;

import com.google.gson.Gson;

/**
 * Created by Bhavsar, Dhara on 10/18/16.
 */

public class ProfileData {

    private String firstName;
    private String lastName;
    private String contactNumber;
    private String streetAddress;
    private String city;
    private String zipCode;
    private String state;
    private String country;

    public ProfileData() {
    }

    public ProfileData(String firstName, String lastName, String contactNumber, String streetAddress, String city, String zipCode, String state, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
        this.streetAddress = streetAddress;
        this.city = city;
        this.zipCode = zipCode;
        this.state = state;
        this.country = country;
    }

    public ProfileData(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public ProfileData(String streetAddress, String city, String zipCode, String state, String country) {
        this.streetAddress = streetAddress;
        this.city = city;
        this.zipCode = zipCode;
        this.state = state;
        this.country = country;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static ProfileData fromJson(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, ProfileData.class);
    }
}
