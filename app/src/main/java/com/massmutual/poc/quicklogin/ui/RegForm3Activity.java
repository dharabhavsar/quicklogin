package com.massmutual.poc.quicklogin.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.massmutual.poc.quicklogin.BuildConfig;
import com.massmutual.poc.quicklogin.R;
import com.massmutual.poc.quicklogin.datamodel.BelongsTo;
import com.massmutual.poc.quicklogin.datamodel.Location;
import com.massmutual.poc.quicklogin.datamodel.Name;
import com.massmutual.poc.quicklogin.datamodel.Result;
import com.massmutual.poc.quicklogin.datamodel.ReversePhoneResponse;
import com.massmutual.poc.quicklogin.service.FullContactService;
import com.massmutual.poc.quicklogin.service.ReversePhoneService;
import com.massmutual.poc.quicklogin.utils.InputUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class RegForm3Activity extends AppCompatActivity {

    private static final String TAG = RegForm3Activity.class.getSimpleName();

    private ProgressDialog progressDialog;
    private TextInputEditText firstName;
    private TextInputEditText lastName;
    private TextInputEditText contactNumber;
    private TextInputEditText streetAddress;
    private TextInputEditText city;
    private TextInputEditText zipCode;
    private TextInputEditText state;
    private TextInputEditText country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_form3);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventBus.getDefault().register(this);

        firstName = (TextInputEditText) findViewById(R.id.first_name_text);
        lastName = (TextInputEditText) findViewById(R.id.last_name_text);
        contactNumber = (TextInputEditText) findViewById(R.id.phone_number_text);
        streetAddress = (TextInputEditText) findViewById(R.id.street_address_text);
        city = (TextInputEditText) findViewById(R.id.city_text);
        zipCode = (TextInputEditText) findViewById(R.id.zip_code_text);
        state = (TextInputEditText) findViewById(R.id.state_text);
        country = (TextInputEditText) findViewById(R.id.country_text);

        progressDialog = new ProgressDialog(this);

        InputUtils.setupSoftInputHide(findViewById(R.id.testLayoutOnClick));

        Intent intent = getIntent();
        if (intent != null) {
            String phoneNo = (String) intent.getExtras().get("PHONE_NUMBER");
//            FullContactService.fetchFullContactData("https://api.fullcontact.com/v2/person.json?phone=+15086541698&apiKey="+ BuildConfig.FULLCONTACT_API_KEY);
//            "+18623242335"
            FullContactService.fetchFullContactInfo(phoneNo);
            progressDialog.setTitle("Fetching Data...");
            progressDialog.show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResponseEvent(PersonResponse response) {
        EventBus.getDefault().removeStickyEvent(response);
        Log.e(TAG, "onResponseEvent: Success" + response.toString());
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onErrorResponse(String error) {
        Log.e(TAG, "onErrorResponse: " + error);
        EventBus.getDefault().removeStickyEvent(error);
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Error Encountered... " + error);
        builder.show();
    }
}
