package com.massmutual.poc.quicklogin.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Bhavsar, Dhara on 10/10/16.
 */

public class FbProfilesAdapter extends RecyclerView.Adapter<FbProfilesAdapter.ViewHolder>  {

    private static final String TAG = FbProfilesAdapter.class.getSimpleName();
    private Context mContext;
    private String mName;
    private String mId;

    public FbProfilesAdapter(Context context, String name, String id) {
        this.mContext = context;
        this.mName = name;
        this.mId = id;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
